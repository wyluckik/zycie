#include "zestaw_zasad.h"
#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>
#include "rysowanie.h"

void zestaw_zasad(char siatka [][81], int l_gen, int czest) /* generuje kolejne plansze dwuwymiarowe */
{
    int i, j, k;
    char kopia[40][81];
    int zywy = 0;
    char buffor[200];
    int licznik = 0;

/*    for(k = 0; k < 40; k++)
    {
	for(j = 0; j < 81; j++)
	    printf("%c", siatka[k][j]);
    	printf("\n");
    }*/

    for(k = 0; k < l_gen; k++) /* rozpoczyna siê zabawa z s¿siedztwem More'a */
    {
        for(i = 0; i < 40; i++)
            for(j = 0; j < 81; j++)
                kopia[i][j] = siatka[i][j];
        for(i = 0; i < 40; i++)
            for(j = 0; j < 81; j++)
            {
                zywy = 0;
                if(i > 0)
                {
                    if((j > 0) && (kopia[i - 1][j - 1] == '#'))
                        ++zywy;
                    if(kopia[i - 1][j] == '#')
                        ++zywy;    
                    if((j < 80) && (kopia[i - 1][j + 1] == '#'))
                        ++zywy;
                }
                if((j > 0) && (kopia[i][j - 1] == '#'))
                    ++zywy;
                if((j < 80) && (kopia[i][j + 1] == '#'))
                    ++zywy;
                if(i < 39)
                {
                    if((j > 0) && (kopia[i + 1][j - 1] == '#'))
                        ++zywy;
                    if(kopia[i + 1][j] == '#')
                        ++zywy;
                    if((j < 80) && (kopia[i + 1][j + 1] == '#'))
                        ++zywy;
                }
                if((zywy == 3) && (kopia[i][j] == ' '))
                    siatka[i][j] = '#';
                else if(kopia[i][j] == ' ')
                    siatka[i][j] = ' ';
                if((zywy == 2 || zywy == 3) && (kopia[i][j] == '#'))
                    siatka[i][j] = '#';
                else if(kopia[i][j] == '#')
                    siatka[i][j] = ' ';
            }
            if(k % czest == 0)
   	    {
   	    process_file(siatka, 40);
   	    sprintf(buffor, "obrazek%d.png", licznik);    
   	    write_png_file(buffor);
	    ++licznik;
    	    }
    }
}
