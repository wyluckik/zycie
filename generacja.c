#include "generacja.h"
#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>

void generacja(int opcja, char siatka[][81], int a) /* wczytuje pierwsz¿ generacj¿; a - oznacza opcj¿ pliku z danymi startowymi*/
{
    FILE *we;
    int i, j, k, l;
    
    if(opcja == 1)
        we = fopen("szybowiec.txt", "r");
    if(opcja == 2)
        we = fopen("fontanna.txt", "r");
    if(opcja == 3)
	we = fopen("moje_dane.txt", "r");
    for(i = 0; i < a; i++)
    {
        if(fscanf(we, "%c", &siatka[i][0]) == EOF)
        {
            for(k = i; k < a; k++)
                for(l = 1; l < 81; l++)
                    siatka[k][l] = ' ';
            break;
        }
        else if(siatka[i][0] == '\n')
        {
            for(l = 1; l < 81; l++)
                siatka[i][l] = ' ';
            continue;
        }
        for(j = 1; j < 81; j++)
        {
            if(fscanf(we, "%c", &siatka[i][j]) == EOF)
            {
                for(k = j; k < 81; k++)
                    siatka[i][k] = ' ';
                j = 81;
                break;
            } 
            if(siatka[i][j] == '\n')
            {
                for(k = j; k < 81; k++)
                    siatka[i][k] = ' ';
                j = 81;
            }
        } 
        /*for(j = 0; j < 81; j++)
            fscanf(we, "%c", &siatka[i][j]); gdyby nie dzia¿a¿a ta pierwsza opcja wczytywania, mo¿na wybra¿ t¿, nieobs¿ugujac¿ 
                                                 EOF i znaków nowej linii */
    }
    fclose(we);
}
