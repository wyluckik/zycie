#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <png.h>
void write_png_file(char* file_name);
void process_file(char plansza[][81], int wysokosc);
void zestaw_zasad(char siatka [][81], int l_gen);
void generacja(int opcja, char siatka[][81], int a);
int main(int argc, char *argv[])
{
    char tablica[40][81];
    
    generacja(atoi(argv[2]), tablica, 40);
    zestaw_zasad(tablica, 10); 

    return 0;
}

void generacja(int opcja, char siatka[][81], int a) /* wczytuje pierwsz¿ generacj¿; a - oznacza opcj¿ pliku z danymi startowymi*/
{
    FILE *we;
    int i, j, k, l;
    
    if(opcja == 1)
        we = fopen("dane_moje.txt", "r");
    if(opcja == 2)
        we = fopen("fontanna.txt", "r");
    for(i = 0; i < a; i++)
    {
        if(fscanf(we, "%c", &siatka[i][0]) == EOF)
        {
            for(k = i; k < a; k++)
                for(l = 1; l < 81; l++)
                    siatka[k][l] = ' ';
            break;
        }
        else if(siatka[i][0] == '\n')
        {
            for(l = 1; l < 81; l++)
                siatka[i][l] = ' ';
            continue;
        }
        for(j = 1; j < 81; j++)
        {
            if(fscanf(we, "%c", &siatka[i][j]) == EOF)
            {
                for(k = j; k < 81; k++)
                    siatka[i][k] = ' ';
                j = 81;
                break;
            } 
            if(siatka[i][j] == '\n')
            {
                for(k = j; k < 81; k++)
                    siatka[i][k] = ' ';
                j = 81;
            }
        } 
        /*for(j = 0; j < 81; j++)
            fscanf(we, "%c", &siatka[i][j]); gdyby nie dzia¿a¿a ta pierwsza opcja wczytywania, mo¿na wybra¿ t¿, nieobs¿ugujac¿ 
                                                 EOF i znaków nowej linii */
    }
    fclose(we);
}

void zestaw_zasad(char siatka [][81], int l_gen) /* generuje kolejne plansze dwuwymiarowe */
{
    int i, j, k;
    char kopia[40][81];
    int zywy = 0;

    for(k = 0; k < 40; k++)
    {
	for(j = 0; j < 81; j++)
	    printf("%c", siatka[k][j]);
    	printf("\n");
    }

    for(k = 0; k < l_gen; k++) /* rozpoczyna siê zabawa z s¿siedztwem More'a */
    {
        for(i = 0; i < 40; i++)
            for(j = 0; j < 81; j++)
                kopia[i][j] = siatka[i][j];
        for(i = 0; i < 40; i++)
            for(j = 0; j < 81; j++)
            {
                zywy = 0;
                if(i > 0)
                {
                    if((j > 0) && (kopia[i - 1][j - 1] == '#'))
                        ++zywy;
                    if(kopia[i - 1][j] == '#')
                        ++zywy;    
                    if((j < 80) && (kopia[i - 1][j + 1] == '#'))
                        ++zywy;
                }
                if((j > 0) && (kopia[i][j - 1] == '#'))
                    ++zywy;
                if((j < 80) && (kopia[i][j + 1] == '#'))
                    ++zywy;
                if(i < 39)
                {
                    if((j > 0) && (kopia[i + 1][j - 1] == '#'))
                        ++zywy;
                    if(kopia[i + 1][j] == '#')
                        ++zywy;
                    if((j < 80) && (kopia[i + 1][j + 1] == '#'))
                        ++zywy;
                }
                if((zywy == 3) && (kopia[i][j] == ' '))
                    siatka[i][j] = '#';
                else if(kopia[i][j] == ' ')
                    siatka[i][j] = ' ';
                if((zywy == 2 || zywy == 3) && (kopia[i][j] == '#'))
                    siatka[i][j] = '#';
                else if(kopia[i][j] == '#')
                    siatka[i][j] = ' ';
            }
    process_file(siatka, 40);
    }
    write_png_file("out.png");
}

int x, y;

int width, height;
png_byte color_type;
png_byte bit_depth;

png_structp png_ptr;
png_infop info_ptr;
int number_of_passes;
png_bytep * row_pointers;

void process_file(char plansza[][81], int wysokosc) {
  width = 81;
  height = wysokosc;
  bit_depth = 8;
  color_type = PNG_COLOR_TYPE_GRAY;

  number_of_passes = 7;
  row_pointers = (png_bytep*) malloc(sizeof(png_bytep) * height);
  for (y=0; y<height; y++)
    row_pointers[y] = (png_byte*) malloc(sizeof(png_byte) * width);

  for (y=0; y<height; y++) {
    png_byte* row = row_pointers[y];
    for (x=0; x<width; x++) {
      row[x] = (plansza[y][x] == '#') ? 0 : 255;
      printf("Pixel at position [ %d - %d ] has RGBA values: %d\n",
       x, y, row[x]);
    }
  }
}

void write_png_file(char* file_name) {
  FILE *fp = fopen(file_name, "wb");
  if (!fp)
    printf("[write_png_file] File %s could not be opened for writing", file_name);

  png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

  if (!png_ptr)
    printf("[write_png_file] png_create_write_struct failed");

  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
    printf("[write_png_file] png_create_info_struct failed");

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during init_io");

  png_init_io(png_ptr, fp);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during writing header");

  png_set_IHDR(png_ptr, info_ptr, width, height,
   bit_depth, color_type, PNG_INTERLACE_NONE,
   PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during writing bytes");

  png_write_image(png_ptr, row_pointers);

  if (setjmp(png_jmpbuf(png_ptr)))
    printf("[write_png_file] Error during end of write");

  png_write_end(png_ptr, NULL);

  for (y=0; y<height; y++)
    free(row_pointers[y]);
  free(row_pointers);

  fclose(fp);
}
